#thoughts: probably use sieve or faster

primes = []
remainders = []

def genprimes(size):
    sieve = [True] * size
    for i in range(2, size):
        if sieve[i]:
            primes.append(i)
            j = 2 * i
            while j < size:
                sieve[j] = False
                j += i

#doesn't work :/
def genprimes2(size):
    #numbers of iterations before reset
    reset = 2000000
    sieve = [True] * reset
    
    #numbers of iterations since reset
    count = 0
    
    #number of times resetted
    block = 0
    
    for i in range(2, size):
        #segmented 
        if count == reset:
            count = 0
            block += 1
            sieve = [True] * reset
            
            for j in range(len(primes)):
                k = remainders[j]
                while k < reset:
                    sieve[k] = False
                    k += primes[j]
                
                remainders[j] = k % reset
        
        #regular sieve
        r = i % reset
        if sieve[r]:
            primes.append(i)
            j = r + i
            while j < reset:
                sieve[j] = False
                j += i
            remainders.append(j % reset)
        
        count += 1
        
size = 2000000        
genprimes(size)  
print(primes)              
print(sum(primes))

