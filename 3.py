primes = [2, 3, 5, 7]

#generates next prime in list
def next_prime():
  k = primes[-1] + 2

  while True:
    is_prime = True
    for prime in primes:
      if k % prime == 0:
        is_prime = False
    
    if is_prime:
      primes.append(k)
      return
    k += 2

#generates prime factors for n
def prime_factor(n):
  factors = [1]
  for prime in primes:
    if n % prime == 0:
      n /= prime
      factors.append(prime)

  while n >= primes[-1]:
    next_prime()
    if n % primes[-1] == 0:
      n /= primes[-1]
      factors.append(primes[-1])
  
  return factors

print(prime_factor(600851475143)[-1])
