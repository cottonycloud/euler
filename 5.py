primes = [2, 3, 5, 7]

#generates next prime in list
def next_prime():
  k = primes[-1] + 2

  while True:
    is_prime = True
    for prime in primes:
      if k % prime == 0:
        is_prime = False
    
    if is_prime:
      primes.append(k)
      return
    k += 2

#generates prime factors for n
def prime_factor(n): 
  #using dictionary instead of list for counting 
  factors = {1: 0}
  for prime in primes:
    while n % prime == 0:
      n /= prime
      if prime not in factors:
        factors[prime] = 1
      else:
        factors[prime] += 1

  while n >= primes[-1]:
    next_prime()
    while n % primes[-1] == 0:
      n /= primes[-1]
      if primes[-1] not in factors:
        factors[primes[-1]] = 1
      else:
        factors[primes[-1]] += 1
  return factors

#finds lcm for list of numbers
def lcm(l):
  factors = {}
  for n in l:
    temp = prime_factor(n)
    for key in temp:
      if key not in factors or factors[key] < temp[key]:
        factors[key] = temp[key]
  
  lcm = 1
  for n in factors:
    lcm *= n ** factors[n]

  return lcm

print(lcm(range(1, 20)))
