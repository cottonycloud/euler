#sum of first n natural numbers
#triangle numbers
def sum_n(n):
  return n * (n + 1) / 2

#sum of first n natural numbers squared
#pyramidal numbers 
def sum_sq(n):
  return n * (n + 1) * (2*n + 1) / 6


print(sum_n(100) ** 2 - sum_sq(100))
