def solve(mat):
    sol = 1
    
    n = len(mat)
    for i in range(n):
        for j in range(n):
            #row
            if j <= n - 4:
                prod = 1
                for k in range(j, j + 4):
                    prod *= mat[i][k]
            
            #column
            if i <= n - 4:
                prod = 1
                for row in mat[i:i + 4]:
                    prod *= row[j]
                    
                if prod > sol:
                    sol = prod 
            
            #diagonal downright
            if i <= n - 4 and j <= n - 4:
                prod = 1
                for k in range(4):
                    prod *= mat[i + k][j + k]

                if prod > sol:
                    sol = prod
                    
            #diagonal upleft
            if i >= 4 and j <= n - 4:
                prod = 1
                for k in range(4):
                    prod *= mat[i - k][j + k]

                if prod > sol:
                    sol = prod            
    return sol
    
def parse():
    mat = []
    f = open("11.txt", 'r')

    for line in f:
        mat.append(line.split())
    
    mat = [[int(y) for y in x] for x in mat]
    f.close()
    return mat
    
mat = parse()
print(solve(mat))