#returns max prod of adjacent k digits
def maxprod(s, k):
  end = len(s) - 13
  prod = 0
  i = 0
  
  while i < end:
    num = 1  
    for digit in s[i:i + k]:
      num *= int(digit)
    
    if num > prod:
      prod = num

    i += 1
  return prod

f = open("8.txt", 'r')
text = f.read().replace("\n", "")
f.close()
print(maxprod(text, 13))
