#even fib # occur for all 3 | n
#sum(f3n) = (fn2 - 1)/ 2

fibseq = [1, 1]

def main():
  fib(4000000)
  n = len(fibseq)
  fn2 = fibseq[-1]

  if(n % 3 == 0):
    fn2 = fibseq[-2] + 2*fibseq[-1]
  elif(n % 3 == 1):
    fn2 = fibseq[-2] + fibseq[-1]
  print((fn2 - 1)/2)

#generates fib sequence up to most
def fib(most):
  fn2 = 0
  while fn2 < most:
    fn = fibseq[-2]
    fn1 = fibseq[-1]
    fn2 = fn + fn1
    fibseq.append(fn2)

main()
