def is_palindrome(n):
  s = str(n)
  i = 0
  j = len(s) - 1
  while i <= j:
    if s[i] != s[j]:
      return False
    i += 1
    j -= 1

  return True

#finds largest palindrome product of n * n digits
def prod_palindrome(n):
  low = 10**(n - 1)
  high = 10**n - 1
  largest = 0
  
  for a in xrange(low, high):
    for b in xrange(low, high):
      if a <= b:
        prod = a * b
        if prod > largest and is_palindrome(prod):
          largest = prod

  return largest   

print(prod_palindrome(3))
