#thoughts: probably use sieve or faster

primes = [2, 3, 5, 7]

#generates next n primes in list
#modification of #3
def next_prime(n):
  count = len(primes)
  k = primes[-1] + 2

  while count <= n:
    is_prime = True
    for prime in primes:
      if k % prime == 0:
        is_prime = False
    
    if is_prime:
      primes.append(k)
      count += 1
    k += 2

next_prime(10001)
print(primes[10000])
