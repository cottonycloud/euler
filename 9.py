#m, n in Z, m > n
#a = m^2 - n^2
#b = 2mn
#c = m^2 + n^2
#a + b + c = 2m^2 + 2mn = 1000
#so m^2 = 500 is an upper bound

import math

#generates pythagorean triple s.t. a + b + c = num
def make_tuple(num):
  limit = int(math.sqrt(num))
  
  for m in xrange(2, limit):
    n = (num - 2*m**2) / (2 * m)

    if m <= n or n < 0:
      continue
    
    a = m**2 - n**2
    b = 2 * m * n
    c = m**2 + n**2
    
    #scale triple 
    if 1000 % (a + b + c) == 0:
      k = 1000 / (a + b + c)
      a *= k
      b *= k
      c *= k
    
    #check if triple due to int(sqrt())
    if a**2 + b**2 == c**2 and (2 * m**2) + (2 * m * n) == num:
      return a, b, c

a, b, c = make_tuple(1000)
print(a * b * c)

    
